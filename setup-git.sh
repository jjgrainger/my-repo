function setup_git() {
    echo 'Lets create a git repository on Bitbucket...';

    echo -n 'Bitbucket Repo: (team/name) '
    read REPO

    echo -n 'Bitbucket username: '
    read USER

    echo -n 'Bitbucket password: '
    read -s PASS
    echo ''

    res=$(curl -s --user $USER:$PASS -X POST -H "Content-Type: application/json" -d '{"scm": "git" }' https://api.bitbucket.org/2.0/repositories/$REPO)

    if [[ $res =~ '"type": "error"' ]]; then
        echo 'Unable to create repository on Bitbucket :('
        echo $res
        exit;
    else
        echo "New repository at http://bitbucket.org/$REPO"
    fi

    git init
    git remote add origin git@bitbucket.org:$REPO.git
    git add --all
    git commit -m 'initial commit'
    git push -u origin master
}

setup_git
